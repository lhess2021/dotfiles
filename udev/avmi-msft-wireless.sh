#!/usr/bin/sh

#echo "AVMI MSFT Wireless"  >> "/home/lh/udev.log"
# See https://unix.stackexchange.com/questions/612686/udev-not-running-xinput-command-inside-script
export DISPLAY=:0 XAUTHORITY=/home/lh/.Xauthority
/usr/bin/xinput --set-prop 'Microsoft Microsoft® 2.4GHz Transceiver v9.0 Mouse' 'libinput Accel Speed' -0.85
