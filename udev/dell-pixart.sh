#!/usr/bin/sh

#echo "Dell PixArt" >> "/home/lh/udev.log"
# See https://unix.stackexchange.com/questions/612686/udev-not-running-xinput-command-inside-script
export DISPLAY=:0 XAUTHORITY=/home/lh/.Xauthority
/usr/bin/xinput --set-prop 'PixArt Dell MS116 USB Optical Mouse' 'libinput Accel Speed' -0.85
