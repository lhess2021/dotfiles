;; -*- no-byte-compile: t; -*-
;;; $DOOMDIR/packages.el

;; To install a package with Doom you must declare them here and run 'doom sync'
;; on the command line, then restart Emacs for the changes to take effect -- or
;; use 'M-x doom/reload'.


;; To install SOME-PACKAGE from MELPA, ELPA or emacsmirror:
                                        ;(package! some-package)

;; To install a package directly from a remote git repo, you must specify a
;; `:recipe'. You'll find documentation on what `:recipe' accepts here:
;; https://github.com/radian-software/straight.el#the-recipe-format
                                        ;(package! another-package
                                        ;  :recipe (:host github :repo "username/repo"))

;; If the package you are trying to install does not contain a PACKAGENAME.el
;; file, or is located in a subdirectory of the repo, you'll need to specify
;; `:files' in the `:recipe':
                                        ;(package! this-package
                                        ;  :recipe (:host github :repo "username/repo"
                                        ;           :files ("some-file.el" "src/lisp/*.el")))

;; If you'd like to disable a package included with Doom, you can do so here
;; with the `:disable' property:
                                        ;(package! builtin-package :disable t)

;; You can override the recipe of a built in package without having to specify
;; all the properties for `:recipe'. These will inherit the rest of its recipe
;; from Doom or MELPA/ELPA/Emacsmirror:
                                        ;(package! builtin-package :recipe (:nonrecursive t))
                                        ;(package! builtin-package-2 :recipe (:repo "myfork/package"))

;; Specify a `:branch' to install a package from a particular branch or tag.
;; This is required for some packages whose default branch isn't 'master' (which
;; our package manager can't deal with; see radian-software/straight.el#279)
                                        ;(package! builtin-package :recipe (:branch "develop"))

;; Use `:pin' to specify a particular commit to install.
                                        ;(package! builtin-package :pin "1a2b3c4d5e")


;; Doom's packages are pinned to a specific commit and updated from release to
;; release. The `unpin!' macro allows you to unpin single packages...
                                        ;(unpin! pinned-package)
;; ...or multiple packages
                                        ;(unpin! pinned-package another-pinned-package)
;; ...Or *all* packages (NOT RECOMMENDED; will likely break things)
                                        ;(unpin! t)

;; Git diff syntax highlighting in Rust
;; (package! magit-delta :recipe (:host github :repo "dandavison/magit-delta") :pin "56cdffd377279589aa0cb1df99455c098f1848cf")

;; Add colors and fonts to elisp manpages
;; (package! info-colors :pin "47ee73cc19b1049eef32c9f3e264ea7ef2aaf8a5")

;; Show typed keys in the modeline for Emacs demos
;; (package! keycast :pin "72d9add8ba16ae8cfcff7fc050fa75e493b4e")

;; Syntax highlighting for systemd unit files.
;; (package! systemd :pin "b6ae63a236605b1c5e1069f7d3afe06ae32a7bae")

;; Use Latex in calculator mode
;; (package! calctex :recipe (:host github :repo "johnbcoughlin/calctex"
;;                            :files ("*.el" "calctex/*.el" "calctex-contrib/*.el" "org-calctex/*.el" "vendor"))
;;   :pin "67a2e76847a9ea9eff1f8e4eb37607f84b380ebb")

;; Pretty tables in org
(package! org-pretty-table
  :recipe (:host github :repo "Fuco1/org-pretty-table") :pin "7bd68b420d3402826fea16ee5099d04aa9879b78")

;; Import to org with pandoc
(package! org-pandoc-import
  :recipe (:host github
           :repo "tecosaur/org-pandoc-import"
           :files ("*.el" "filters" "preprocessors")))

;; Automatic org latex rendering
;; (package! org-fragtog :pin "680606189d5d28039e6f9301b55ec80517a24005")

;; (package! lsp-treemacs  :pin "2894e6dec583eaa77037627e9d8c3bc89cf7273d")

;; (package! dashboard)

(package! org-auto-tangle)

;; Easily open very large files by loading them in chunks
(package! vlf)

;; Take screenshot and insert PNG into org document at point.
(package! org-screenshot)

;; (package! doom-modeline :pin "918730eff72e")

;; Pin Org mode to 9.6 because new element syntax tree causes a variety of errors
;; (package! org :pin "806abc5a2bbcb5f884467a0145547221ba09eb59")

;; Directory sizes in dired
(package! dired-du)

;; Rename symbol functionality in elisp
(package! erefactor
  :recipe (:host github
           :repo "mhayashi1120/Emacs-erefactor"))

;; Visualize org agenda in timeblocks
(package! org-timeblock
  :recipe (:host github
           :repo "ichernyshovvv/org-timeblock"))

;; Matlab-mode
(package! matlab-mode)

(package! elisp-autofmt)
;; (package! emacs-elisp-autofmt
;;   :recipe (:host codeberg
;;            :repo "ideasman42/emacs-elisp-autofmt"))

;; Screenshots within Emacs
;; (package! screenshot
;;   :recipe (:host github
;; :repo "tecosaur/screenshot"))

(package! org-present)

;; (package! camcorder
;;   :recipe (:host github
;;            :repo "Malabarba/camcorder.el"))
