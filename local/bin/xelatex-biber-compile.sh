#!/usr/bin/env bash

texFile="$1"
texFileNoExt="$(echo "$texFile" | cut -d'.' -f1)"

xe() {
    xelatex "$texFile"
}

xe && biber "$texFileNoExt" && xe && xe
