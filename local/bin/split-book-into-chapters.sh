#!/usr/bin/env bash

usage() {
    echo "Split a textbook PDF into chapters from a given table of contents file."
    echo " - arg 1: base PDF"
    echo " - arg 2: file TOC"
    echo " - arg 3: pdf OFFSET"
    echo "The TOC file must be a comma-separated-value file as follows: <ch. num>,<page START>,<page END>,"
    echo "For example:"
    echo "1,10,42"
    echo "2,43,76"
    echo "2 - Curvilinear Motion,43,76"
    echo "A1,788,790, -- (e.g. for Appendix 1)"
    echo "This script calls extract-pdf-range.sh for each page range to make a file for each chapter."
    echo "Supports a page offset: difference between page number in pdf and page number in textbook."
    echo -e "\nRun with arg '-h' or '--help' for how to calculate START AND END in TOC"
    echo "and for how to calculate the OFFSET."
    exit 0
}

help() {
    echo -e "1. The START page is the page number assigned to the chapter heading,\n   i.e. the page number of the page on which the chapter begins."
    echo -e "2. The END page is the page number of the START page of the next chapter, minus one.\n   You cannot simply use the page number of the last heading in the current chapter because\nit is possible that the last heading may be more than one page."
    echo -e "3. The OFFSET is defined by the formula: BOOK_PAGE_NUM + OFFSET = PDF_PAGE_NUM.\n   If page 1 of the book is page 21 of the PDF, then OFFSET = 21 - 1 = 20."
    echo -e "4. The END of the last chapter should be given as END-OFFSET so that when OFFSET is added\n   the page number doesn't exceed the total number of pages in the PDF."
    exit 0
}

# toc END should be PG - 1 where PG is first page of next chapter
# offset

# Fail if arguments aren't supplied.
[[ -z $1 ]] && usage

# Check for help argument.
[[ $1 == "-h" ]] || [[ $1 == "--help" ]] && help

# Ensure second and third arguments are supplied if first arg isn't help.
[[ -z $2 ]] || [[ -z $3 ]] && usage

# Assign args
PDF="$1"
TOC="$2"
OFFSET="$3"

# Iterate through TOC and call extract-pdf-range.sh for each line.
while IFS="" read -r line || [ -n "$line" ]
do
    CH_NAME="$(echo "$line" | awk -F',' '{print $1}')"

    PRE_OFFSET_START="$(echo "$line" | awk -F',' '{print $2}')"
    PRE_OFFSET_END="$(echo "$line" | awk -F',' '{print $3}')"
    START=$(( PRE_OFFSET_START + OFFSET ))
    END=$(( PRE_OFFSET_END + OFFSET ))

    OUTFILE="ch-${CH_NAME}_$PDF"
    extract-pdf-range.sh "$PDF" $START $END "$OUTFILE"
done < "$TOC"
