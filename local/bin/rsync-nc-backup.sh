#!/usr/bin/env bash

# With trailing slash on src/ and dest/, copies contents of src/ under dest/.
# See https://explainshell.com/explain?cmd=rsync+-chavzP+--stats+user%40remote.host%3A%2Fpath%2Fto%2Fcopy+%2Fpath%2Fto%2Flocal%2Fstorage for explanation of options.
rsync -chavzP --stats pi@192.168.0.20:/path/to/src/ /path/to/dest/
