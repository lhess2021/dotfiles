#!/usr/bin/env bash

# Quality-of-Life handler for sioyek

_file="$1"

_sioyek() {
    # Call sioyek on $1 and set window title to $1
    # sioyek --title "$1" --new-window "$1" &
    # And hide all sioyek's output
    sioyek --title "$1" "$1" >/dev/null 2>&1 &
}

# Determine if sioyek is being called on file with or without PDF extension
if [[ ${_file} == *.pdf ]]; then
    # File has PDF extension, so call sioyek on the file as normal.
    _sioyek "${_file}"
else
    # File doesn't have PDF extension.
    # First, check if the file happens to be a PDF with a non-PDF extension
    mime_type="$(file -b --mime-type "${_file}")"
    if [[ $mime_type == application/pdf ]]; then
        # File is indeed a PDF, so call sioyek as normal
        _sioyek "${_file}"
    else
        # File needs PDF extension, so append it and call sioyek
        _sioyek "${_file}.pdf"
    fi
fi
