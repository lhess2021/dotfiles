#!/usr/bin/env bash

echo "$(date)" >> "$HOME/date.txt"
cd /home/user/nextcloud-sync/.todo.bak && git add todo.org && git commit -m "Hourly todo.org backup commit"
