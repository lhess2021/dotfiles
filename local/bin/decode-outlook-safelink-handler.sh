#!/usr/bin/env bash

# Decode Outlook Safelinks in the clipboard with the usl tool and copy the
# actual URL back to the clipboard.
#
# Depends on the usl tool: https://github.com/atc0005/safelinks

# Trim trailing newline from clipboard and pipe to USL to decode URL.
# Redirect USL stderr to stdout so that if URL decoding fails, you'll see the
# error message when you try to paste the decoded URL.
decodedUrl="$(xclip -sel c -o | tr -d '\n' | usl 2>&1)"

# Trim the newline that USL adds and put in clipboard.
# QubesOS specific: when using qvm-run to run a script with xsel, qvm-run
# simply exits after piping to xsel. When piping to xclip, however, qvm-run
# waits until you paste (sometimes even after).
echo "$decodedUrl" | tr -d '\n' | xsel -i -b
