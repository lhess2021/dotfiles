#!/usr/bin/env bash

# Replace curly quotes (‘,’,“,”) with straight quotes (',") using sed, in place.
# Uses first argument as file to edit.
# Useful to converting some "fancy" text copied from e.g. Word to LaTeX.

echo "Make sure file '$1' is saved in your editor! sed will do an in-place substitution."
echo "Sleeping three seconds to let you save or stop this script."

sleep 3

sed -i "s/‘/'/g" "$1"
sed -i "s/’/'/g" "$1"
sed -i 's/“/"/g' "$1"
sed -i 's/”/"/g' "$1"

echo "Done."
