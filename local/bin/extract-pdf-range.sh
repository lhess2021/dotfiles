#!/usr/bin/env bash

usage() {
    echo "Extract a page range from a PDF using pdftk."
    echo " - arg 1: base PDF"
    echo " - arg 2: page START"
    echo " - arg 3: page END"
    echo " - arg 4 (optional): filename OUTPUT"
    echo "With no 4th argument, the output file is: pg-START-END-PDF"
    echo "Command which will be run: pdftk <PDF> cat <START>-<END> output <OUTFILE>"
    exit 1
}

# Extract a page range from a PDF using pdftk
FILE=$1
START=$2
END=$3

# Fail if first 3 args are empty.
[[ -z $FILE ]] || [[ -z $START ]] || [[ -z $END ]] && usage

# Set output file if not supplied as arg.
OUTFILE=$4
[[ -z $4 ]] && OUTFILE="pg-$START-$END-$FILE"

pdftk "$FILE" cat "$START"-"$END" output "$OUTFILE"
