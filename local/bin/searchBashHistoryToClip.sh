#!/usr/bin/env bash

# Grep the bash history for a string and ask the user which of the results to
# copy to the clipboard

string="$1"

# Search for string in bash history with line numbers
results="$(grep -n "$string" /home/user/.bash_history)"

# Print search results
echo "$results"

# Get number of result
read -p "Number of result to copy to clipboard: " resultNum

# Get the result from results
result="$(echo "$results" | grep "$resultNum" | cut -d ':' -f2)"

# Copy result to clipboard
echo "$result" | xclip -sel c
