" .sls to yaml filetype file
if exists("did_load_filetypes")
	finish
endif
augroup filetypedetect
	au! BufRead,BufNewFile *.sls		setfiletype yaml
augroup END
