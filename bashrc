# Colors
TERM="xterm-256color" # getting proper colors
PS1='\[[0;1;38;5;34m\]\u\[[0;1;38;5;34m\]@\[[0;1;38;5;34m\]\h\[[0;1;38;5;57m\]:\[[0;1;38;5;57m\]\W \[[0;1;38;5;57m\]$ \[[0m\]' # green and blue

# Env vars
export EDITOR=vim
export MANPAGER='bat -l man -p'
export HISTCONTROL=ignoreboth # don't put commands duplicate or starting with space in history

# Append to history file, don't overwrite
shopt -s histappend
export HISTSIZE=10000

# Update window size after each command
shopt -s checkwinsize

# Vi mode
set -o vi
# Have C-l clear screen also in command mode: https://unix.stackexchange.com/a/104101
bind -m vi-insert 'Control-l: clear-screen'

# Bracketed paste
#set enable-bracketed-paste on

# Aliases
alias ls='eza'
alias l='eza --color=always --group-directories-first -t=modified'
alias ll='eza -l --color=always --group-directories-first -t=modified'
alias la='eza -la --color=always --group-directories-first -t=modified'
alias ld='eza -lad */ .*/ --color=always --group-directories-first -t=modified'
alias cl='clear'
alias v='vim'
alias df='df -h'
alias bat='bat'
alias py='python3'
alias gits='git status'
alias gitl='git log --graph --decorate --oneline'
alias gitz='git count-objects -vH'
alias emacs-kill='systemctl --user stop emacs.service && systemctl --user status emacs.service'
alias emacs-restart='systemctl --user restart emacs.service && systemctl --user status emacs.service'
alias emacs-stop='systemctl --user stop emacs.service && systemctl --user status emacs.service'
alias emacs-start='systemctl --user start emacs.service && systemctl --user status emacs.service'
alias emacs-status='systemctl --user status emacs.service'
alias emacs2='setsid emacs'
alias py='python3'
alias lmk='latexmk -pdfxe'
alias lmks='latexmk -shell-escape -pdfxe'
alias top='htop'
alias toclip='xsel -b'
alias psagrep='ps aux | grep'
alias lsusb='cyme -l' # modern alternative to lsusb
alias grep='grep -i' # case insensitive grep

# Grep bash history
greph() {
        grep "$1" $HOME/.bash_history
}

lfcd () {
    # `command` is needed in case `lfcd` is aliased to `lf`
    cd "$(command lf-ueberzug -print-last-dir "$@")"
}
alias lf='lfcd'

# Open to directory
if [[ -f /usr/bin/lf ]]; then
  if [[ ! -z "$LF_DIR" ]]; then
    cd "$LF_DIR" || exit
  fi
fi

# Navigation
up() {
        local d=""
        local limit="$1"

        if [ -z "$limit" ] || [ "$limit" -le 0 ]; then
                limit=1
        fi

        for ((i=1; i<=limit; i++)); do
                d="../$d"
        done

        cd "$d" || exit
}

export PATH="$HOME/nextcloud-sync/projects/ext.d/lf-gadgets/lf-ueberzug:$HOME/.local/bin:$HOME/.config/emacs/bin:$PATH"
#export PATH="$HOME/go/bin/:$HOME/.cargo/bin:$HOME/.cabal/bin:$HOME/.local/bin:$HOME/.emacs.d/bin:$PATH"
#export PYTHONPATH="/usr/local/lib/python3.9/site-packages/cv2:$PYTHONPATH"

# PATH="/home/user/perl5/bin${PATH:+:${PATH}}"; export PATH;
# PERL5LIB="/home/user/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
# PERL_LOCAL_LIB_ROOT="/home/user/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
# PERL_MB_OPT="--install_base \"/home/user/perl5\""; export PERL_MB_OPT;
# PERL_MM_OPT="INSTALL_BASE=/home/user/perl5"; export PERL_MM_OPT;

source "/usr/share/bashmarks/bashmarks.sh" # define install location with yay
# . "/home/user/.cargo/env"

# bun
# export BUN_INSTALL="$HOME/.local/share/reflex/bun"
# export PATH="$BUN_INSTALL/bin:$PATH"
xset r rate 300 50

# Starship
eval "$(starship init bash)"
#source "/home/user/.local/bin/bashmarks.sh"
#. "/home/user/.cargo/env"

# bun
#export BUN_INSTALL="$HOME/.local/share/reflex/bun"
#export PATH="$BUN_INSTALL/bin:$PATH"
